#include <iostream>
#include "main.h"

using namespace std;

Writer::Writer( Manager * manager ) :
    manager( manager )
{
    running = false;
    next_key_ordinal = 0;
    max_key_ordinal = 0;
    stats.start_time = chrono::high_resolution_clock::now();
}

Writer::~Writer()
{
    stop();    
}

int Writer::getMaxKeyOrdinal()
{
    return max_key_ordinal;
}

std::string Writer::keyToString( long key )
{
    char str[32];
    snprintf (str, 32, "%s%010ld", key_prefix.c_str(), key );
    return string(str);
}

std::string Writer::getKey( long ordinal )
{
    return keyToString( ordinalToKey( ordinal ) );
}

long Writer::ordinalToKey( long ordinal )
{
    assert(ordinal >= 0);

    switch( key_strategy ) 
    {
    case SEQUENTIAL:
        break;

    case CLUSTERED: {
        long o = ordinal;
        long hi = ordinal >> 10;
        long lo = ordinal & 0x3FF;
        ordinal = ((hi * MED_PRIME) & 0x1FFFFF) << 10;
        ordinal |= (lo * SMALL_PRIME) & 0x3FF;
        break;
    }

    case SCATTERED:
        ordinal = (ordinal * BIG_PRIME) & 0x7FFFFFFF;
        break;

    default:
        break;
    }

    return ordinal;
}

void Writer::start()
{
    if( !running ) {
        stats = Stats();  // Reset stats
        stats.start_time = chrono::high_resolution_clock::now();
        running = true;
        thread = std::thread(worker, this);
    }
}

void Writer::stop()
{
    if( running ) {
        running = false;
        thread.join();
        stats.end_time = chrono::high_resolution_clock::now();
        stats.dumpStatistics( key_prefix + "writer" );
    }
}

void Writer::put( long key, std::string value )
{
    chrono::time_point<chrono::high_resolution_clock> start = chrono::high_resolution_clock::now();
    put( keyToString( key ), value );
    chrono::time_point<chrono::high_resolution_clock> end = chrono::high_resolution_clock::now();
    chrono::duration<double> duration = end - start;

    stats.execution_time += duration;
    stats.write_count++;
    int size = value.size();
    stats.bytes_written += size;
    
    unique_lock<mutex> lock( manager->stats_lock );
    manager->total_writes++;
    if( key > manager->max_key_written ) {
        manager->max_key_written = key;
    }
    manager->stats.execution_time += duration;
    manager->stats.write_count++;
    manager->stats.bytes_written += size;

    //cout << "insert " << key << "=" << value << endl; 
}

void Writer::putMany( long count )
{
    for( long i = 0; i < count; i++ ) {
        doWrite();
        if( (i & 0xFFFFF) == 0 ) {
            cout << "misc,putMany_progress," << to_string(i+1) << " of " << to_string(count) << endl;
        }
    }
}

void Writer::doWrite()
{
    put( ordinalToKey( next_key_ordinal ), buffer );
    if( next_key_ordinal > max_key_ordinal ) {
        max_key_ordinal = next_key_ordinal;
    }
    next_key_ordinal++;
    if( next_key_ordinal >= wrapAtOrdinal ) {
        next_key_ordinal = 0;
    }
}

void Writer::doWrites()
{
    while(running && throttling_method != DO_NOTHING ) {
        if( throttling_method == RW_RATIO ) {
            while( running ) {
                if( manager->stats.write_count == 0 ) {
                    break;
                }
                int ratio = manager->stats.read_count / manager->stats.write_count;
                if( ratio >= rw_ratio ) {
                    break;
                }
                usleep(10000);
            }
        }

        doWrite();

        if( sleep_us > 0 ) {
            usleep( sleep_us );
        }
    }
}

void Writer::worker( Writer * updater )
{
    updater->doWrites();
}


