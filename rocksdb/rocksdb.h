#ifndef _ROCKSDB_H_
#define _ROCKSDB_H_

#include "rocksdb/db.h"
#include "rocksdb/slice.h"
#include "rocksdb/options.h"
#include "rocksdb/table.h"
#include "rocksdb/filter_policy.h"

#include "main.h"

class RocksDBManager : public Manager
{
    virtual std::unique_ptr<class Reader> newReader();
    virtual std::unique_ptr<class Writer> newWriter();
    virtual void dumpStoreStatistics();

public:
    RocksDBManager();
    virtual ~RocksDBManager();
    std::unique_ptr<rocksdb::DB> db;
    std::vector<rocksdb::ColumnFamilyHandle*> handles;
    std::shared_ptr<rocksdb::Statistics> statistics;
};

class RocksDBWriter : public Writer
{
    rocksdb::DB * const db;

public:
    RocksDBWriter( RocksDBManager * manager );
    virtual void put( std::string const & key, std::string const & value ); 
};

class RocksDBReader : public Reader
{
    rocksdb::DB * const db;

public:
    RocksDBReader( RocksDBManager * manager );
    virtual std::string get( std::string const & key );
    virtual void * rangeBegin( std::string const & key_start, std::string const & key_end ); 
    virtual std::tuple<std::string,std::string> rangeNext( void * cursor );
    virtual void rangeEnd( void * cursor ); 
};

class RocksDBCursor
{
    bool in_range;
    rocksdb::ReadOptions options;
    rocksdb::Slice lower_bound;
    std::string upper_bound;
    std::unique_ptr<rocksdb::Iterator> iter;

    RocksDBCursor( std::string const & key_start, std::string const & key_end );
    friend class RocksDBReader;
};

#endif