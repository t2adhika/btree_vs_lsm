#include <iostream>
#include <string>
#include <cassert>
#include <vector>

#include "rocksdb.h"

using namespace rocksdb;
using namespace std;

int main( int argc, char *argv[] ) {
    RocksDBManager manager;
    return testmain( &manager, argc, argv );  
}

//----------------------------------------------------------------------------

RocksDBManager::RocksDBManager()
    : statistics( CreateDBStatistics() )
{
    BlockBasedTableOptions table_options;
    table_options.filter_policy.reset( NewBloomFilterPolicy(10,false) );
    
    vector<ColumnFamilyDescriptor> column_families;
    ColumnFamilyDescriptor cfd = { kDefaultColumnFamilyName, ColumnFamilyOptions() };
    column_families.push_back( cfd );
    cfd.options.table_factory.reset( NewBlockBasedTableFactory(table_options) );

    DBOptions db_options;

    Options options( db_options, cfd.options );
    options.IncreaseParallelism();
    options.OptimizeLevelStyleCompaction();
    options.compression = kNoCompression;
    options.bottommost_compression = kNoCompression;
    options.create_if_missing = true;
    options.statistics = statistics;
    shared_ptr<Cache> cache = NewLRUCache(4000000000);
    table_options.block_cache = cache;
    options.table_factory.reset( NewBlockBasedTableFactory(table_options) );



    DB * temp;
    //Status s = DB::Open( options, "simpledb", column_families, &handles, &temp );
    Status s = DB::Open( options, "simpledb", &temp );
    db.reset( temp );
    //cout << s.ToString() << endl;
    assert(s.ok());
}

RocksDBManager::~RocksDBManager()
{    
    cout << statistics.get()->ToString() << endl;
}

void RocksDBManager::dumpStoreStatistics()
{
    system("echo store,size,`du -h simpledb | sed 's/\tsimpledb//g'`");
    unsigned long num_keys;
    db->GetAggregatedIntProperty("rocksdb.estimate-num-keys", &num_keys);
    cout << "store,key_count_estimate," << num_keys << endl;
}

unique_ptr<Reader> RocksDBManager::newReader()
{    
    return unique_ptr<Reader>( new RocksDBReader( this ) );
}

unique_ptr<Writer> RocksDBManager::newWriter()
{   
    return unique_ptr<Writer>( new RocksDBWriter( this ) );  
}

//----------------------------------------------------------------------------

RocksDBWriter::RocksDBWriter( RocksDBManager * manager ) :
    Writer(manager),
    db( manager->db.get() )
{
}

void RocksDBWriter::put( std::string const & key, std::string const & value )
{
    // Put key-value
    Status s = db->Put( WriteOptions(), key, value );
    //cout << "Put: " << key << "=" << value << endl;
    assert(s.ok());    
}

//----------------------------------------------------------------------------

RocksDBCursor::RocksDBCursor( string const & key_start, string const & key_end ) :
    lower_bound( key_start ),
    upper_bound( key_end )
{
    options.iterate_lower_bound = &lower_bound;
    //options.iterate_upper_bound = &upper_bound;
    in_range = true;
}

RocksDBReader::RocksDBReader( RocksDBManager * manager ) :
    Reader( manager ),
    db( manager->db.get() )
{
}

// int read_count = 0;
// int missed_read_count = 0;

string RocksDBReader::get( std::string const & key )
{
    string value;
    Status s = db->Get(ReadOptions(), key, &value);
    if( !s.ok() ) {
        value = "";    
    }
    
    // read_count++;
    // if( !s.ok() ) {
    //     missed_read_count++;
    //     if( missed_read_count % 1000 == 0 ) {
    //         cout << "Missed reads " << missed_read_count << " of " << read_count << " " << s.ToString() << endl;
    //     }
    // } else {
    //     //cout << "ok" << endl;
    // }

    return value;
}

void * RocksDBReader::rangeBegin( string const & key_start, string const & key_end )
{
    RocksDBCursor * cursor = new RocksDBCursor( key_start, key_end );
    cursor->iter.reset( db->NewIterator( cursor->options ) );
    cursor->iter->SeekToFirst();
    return cursor;
} 

tuple<string,string> RocksDBReader::rangeNext( void * cursor )
{
    RocksDBCursor * c = (RocksDBCursor*)cursor;
    Iterator * iter = c->iter.get();

    if( iter->Valid() && c->in_range ) {
        tuple<string,string> ret = { iter->key().ToString(), iter->value().ToString() };
        if( std::get<0>( ret ) > c->upper_bound ) {
            c->in_range = false;            
        } else {
            iter->Next();
            return ret;
        }
    }
    return { "", "" };    
}

void RocksDBReader::rangeEnd( void * cursor )
{
    delete (RocksDBCursor*)cursor;
}
