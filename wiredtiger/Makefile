objects = test.o writer.o reader.o manager.o wt.o
wiredtiger_lib = ../../wiredtiger/.libs/libwiredtiger.so
header_dirs = .. . ../../wiredtiger
headers = main.h wiredtiger.h

debug_opts=-DDEBUG -g3
placed_objects = $(addprefix build/,$(objects)) $(wiredtiger_lib)
include_opt = $(addprefix -I, $(header_dirs))
sys_libs = -MT -MD -MP -MF -lrt -ldl -lpthread
VPATH=.:..:build:../../wiredtiger

# Install tools with: sudo apt-get install cgroup-tools
CG=cgexec -g memory:perftest
DUR=100
TR=12
ifeq ($(type),lsm)
#	echo "In the lsm tree"
	PR = 1
else
#	echo "In the btree"
	PR = 0
endif
TEST=./wt
SIZE=100
CLEAN=rm -rf db; mkdir -p db

build/wt: $(placed_objects)
	$(CXX) -o $@ $^ $(sys_libs)

cleandb:
	cd build;  $(CLEAN) 

clean:
	rm -rf build

cgsetup:
	sudo cgcreate -g memory:perftest
	sudo chgrp -R cs848 /sys/fs/cgroup/memory/perftest
	sudo chmod -R g+w /sys/fs/cgroup/memory/perftest
	
cg4g:	
	echo 4000000000 > /sys/fs/cgroup/memory/perftest/memory.limit_in_bytes

cg8g:
	echo 8000000000 > /sys/fs/cgroup/memory/perftest/memory.limit_in_bytes

validate: $(TEST) cleandb
	# readrate threads duration value_size rw_ratio
	cd build; $(CG) $(TEST) $(PR) 2>&1 | tee ../validate-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log

random: $(TEST) cleandb
	# readrate threads read_strategy write_strategy range_size duration value_size rw_ratio
	cd build; $(CG) $(TEST) $(PR) readrate $(TR) scattered clustered 1 100 100 100 2>&1 | tee ../readrate-01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log

random_all: $(TEST)
	# readrate threads read_strategy write_strategy range_size duration value_size rw_ratio
	cd build; $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) scattered clustered 1 100 $(SIZE) 100 2>&1 | tee ../readrate-01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) scattered clustered 1 100 $(SIZE) 25 2>&1 | tee ../readrate-02-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) scattered clustered 1 100 $(SIZE) 10 2>&1 | tee ../readrate-03-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) scattered clustered 1 100 $(SIZE) 5 2>&1 | tee ../readrate-04-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log

writes: $(TEST) cleandb
	cd build; $(CG) $(TEST) $(PR) populate sequential 10000000 $(SIZE) 2>&1 | tee ../populate-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) populate clustered  10000000 $(SIZE) 2>&1 | tee ../populate-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) populate scattered  10000000 $(SIZE) 2>&1 | tee ../populate-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log

populate_clusterd: $(TEST) cleandb
	# populate write_strategy key_count value_size
	cd build; $(CG) $(TEST) $(PR) populate clustered 200000000 $(SIZE) 2>&1 | tee ../populate-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log

populate_sequential: $(TEST) cleandb
	# populate write_strategy key_count value_size
	cd build; $(CG) $(TEST) $(PR) populate sequential 200000000 $(SIZE) 2>&1 | tee ../populate-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log

populate_scattered: $(TEST) cleandb
	# populate write_strategy key_count value_size
	cd build; $(CG) $(TEST) $(PR) populate scattered 20000000 $(SIZE) 2>&1 | tee ../populate-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log

#run populate first
popclustered: $(TEST)
	# readrate threads read_strategy write_strategy range_size duration value_size rw_ratio
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) ranged clustered 1000 $(DUR) $(SIZE) 100 2>&1 | tee ../popclustered-B01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) ranged clustered 1000 $(DUR) $(SIZE) 25 2>&1 | tee ../popclustered-B02-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) ranged clustered 1000 $(DUR) $(SIZE) 10 2>&1 | tee ../popclustered-B03-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) ranged clustered 1000 $(DUR) $(SIZE) 5 2>&1 | tee ../popclustered-B04-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) sequential clustered 1 $(DUR) $(SIZE) 100 2>&1 | tee ../popclustered-C01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) sequential clustered 1 $(DUR) $(SIZE) 25 2>&1 | tee ../popclustered-C02-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) sequential clustered 1 $(DUR) $(SIZE) 10 2>&1 | tee ../popclustered-C03-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) sequential clustered 1 $(DUR) $(SIZE) 5 2>&1 | tee ../popclustered-C04-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) scattered clustered 1 $(DUR) $(SIZE) 100 2>&1 | tee ../popclustered-D01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) scattered clustered 1 $(DUR) $(SIZE) 25 2>&1 | tee ../popclustered-D02-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) scattered clustered 1 $(DUR) $(SIZE) 10 2>&1 | tee ../popclustered-D03-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) scattered clustered 1 $(DUR) $(SIZE) 5 2>&1 | tee ../popclustered-D04-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log

#run populate first
popclustered2: $(TEST)
	# readrate threads read_strategy write_strategy range_size duration value_size rw_ratio
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) ranged clustered 1000 $(DUR) $(SIZE) 4 2>&1 | tee ../popclustered-B01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) ranged clustered 1000 $(DUR) $(SIZE) 3 2>&1 | tee ../popclustered-B02-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) ranged clustered 1000 $(DUR) $(SIZE) 2 2>&1 | tee ../popclustered-B03-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) ranged clustered 1000 $(DUR) $(SIZE) 1 2>&1 | tee ../popclustered-B04-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) sequential clustered 1 $(DUR) $(SIZE) 4 2>&1 | tee ../popclustered-C01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) sequential clustered 1 $(DUR) $(SIZE) 3 2>&1 | tee ../popclustered-C02-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) sequential clustered 1 $(DUR) $(SIZE) 2 2>&1 | tee ../popclustered-C03-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) sequential clustered 1 $(DUR) $(SIZE) 1 2>&1 | tee ../popclustered-C04-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) scattered clustered 1 $(DUR) $(SIZE) 4 2>&1 | tee ../popclustered-D01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) scattered clustered 1 $(DUR) $(SIZE) 3 2>&1 | tee ../popclustered-D02-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) scattered clustered 1 $(DUR) $(SIZE) 2 2>&1 | tee ../popclustered-D03-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) scattered clustered 1 $(DUR) $(SIZE) 1 2>&1 | tee ../popclustered-D04-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log

#run populate first
popsequential: $(TEST)
	# readrate threads read_strategy write_strategy range_size duration value_size rw_ratio
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) sequential sequential 1 $(DUR) $(SIZE) 100 2>&1 | tee ../popsequential-A01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) sequential sequential 1 $(DUR) $(SIZE) 25 2>&1 | tee ../popsequential-A02-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) sequential sequential 1 $(DUR) $(SIZE) 10 2>&1 | tee ../popsequential-A03-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) sequential sequential 1 $(DUR) $(SIZE) 5 2>&1 | tee ../popsequential-A04-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) ranged sequential 1000 $(DUR) $(SIZE) 100 2>&1 | tee ../popsequential-B01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) ranged sequential 1000 $(DUR) $(SIZE) 25 2>&1 | tee ../popsequential-B02-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) ranged sequential 1000 $(DUR) $(SIZE) 10 2>&1 | tee ../popsequential-B03-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) ranged sequential 1000 $(DUR) $(SIZE) 5 2>&1 | tee ../popsequential-B04-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) clustered sequential 1 $(DUR) $(SIZE) 100 2>&1 | tee ../popsequential-C01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) clustered sequential 1 $(DUR) $(SIZE) 25 2>&1 | tee ../popsequential-C02-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) clustered sequential 1 $(DUR) $(SIZE) 10 2>&1 | tee ../popsequential-C03-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) clustered sequential 1 $(DUR) $(SIZE) 5 2>&1 | tee ../popsequential-C04-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) scattered sequential 1 $(DUR) $(SIZE) 100 2>&1 | tee ../popsequential-D01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) scattered sequential 1 $(DUR) $(SIZE) 25 2>&1 | tee ../popsequential-D02-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) scattered sequential 1 $(DUR) $(SIZE) 10 2>&1 | tee ../popsequential-D03-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) scattered sequential 1 $(DUR) $(SIZE) 5 2>&1 | tee ../popsequential-D04-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log

#run populate first
popscattered: $(TEST)
	# readrate threads read_strategy write_strategy range_size duration value_size rw_ratio
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) ranged scattered 1000 $(DUR) $(SIZE) 100 2>&1 | tee ../popscattered-B01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) ranged scattered 1000 $(DUR) $(SIZE) 25 2>&1 | tee ../popscattered-B02-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) ranged scattered 1000 $(DUR) $(SIZE) 10 2>&1 | tee ../popscattered-B03-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) ranged scattered 1000 $(DUR) $(SIZE) 5 2>&1 | tee ../popscattered-B04-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) clustered scattered 1 $(DUR) $(SIZE) 100 2>&1 | tee ../popscattered-C01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) clustered scattered 1 $(DUR) $(SIZE) 25 2>&1 | tee ../popscattered-C02-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) clustered scattered 1 $(DUR) $(SIZE) 10 2>&1 | tee ../popscattered-C03-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) clustered scattered 1 $(DUR) $(SIZE) 5 2>&1 | tee ../popscattered-C04-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) scattered scattered 1 $(DUR) $(SIZE) 100 2>&1 | tee ../popscattered-D01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) scattered scattered 1 $(DUR) $(SIZE) 25 2>&1 | tee ../popscattered-D02-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) scattered scattered 1 $(DUR) $(SIZE) 10 2>&1 | tee ../popscattered-D03-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CG) $(TEST) $(PR) readrate2 $(TR) scattered scattered 1 $(DUR) $(SIZE) 5 2>&1 | tee ../popscattered-D04-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log


sequential: $(TEST)
	# readrate threads read_strategy write_strategy range_size duration value_size rw_ratio
	cd build; $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) sequential sequential 1 $(DUR) $(SIZE) 100 2>&1 | tee ../readrate-01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log

sequential_all: $(TEST)
	# readrate threads read_strategy write_strategy range_size duration value_size rw_ratio
	cd build;  $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) sequential sequential 1 $(DUR) $(SIZE) 100 2>&1 | tee ../readrate-01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) sequential sequential 1 $(DUR) $(SIZE) 25 2>&1 | tee ../readrate-02-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) sequential sequential 1 $(DUR) $(SIZE) 10 2>&1 | tee ../readrate-03-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) sequential sequential 1 $(DUR) $(SIZE) 5 2>&1 | tee ../readrate-04-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log

range: $(TEST)
	# readrate threads read_strategy write_strategy range_size duration value_size rw_ratio
	cd build; $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) ranged sequential 1 $(DUR) $(SIZE) 100 2>&1 | tee ../readrate-01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log

range_all: $(TEST)
	# readrate threads read_strategy write_strategy range_size duration value_size rw_ratio
	cd build; $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) ranged clustered 1 $(DUR) $(SIZE) 100 2>&1 | tee ../readrate-01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) ranged clustered 1 $(DUR) $(SIZE) 25 2>&1 | tee ../readrate-02-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) ranged clustered 1 $(DUR) $(SIZE) 10 2>&1 | tee ../readrate-03-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) ranged clustered 1 $(DUR) $(SIZE) 5 2>&1 | tee ../readrate-04-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log

clustered_all: $(TEST)
	# readrate threads read_strategy write_strategy range_size duration value_size rw_ratio
	cd build; $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) clustered clustered 1 $(DUR) $(SIZE) 100 2>&1 | tee ../readrate-01-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) clustered clustered 1 $(DUR) $(SIZE) 25 2>&1 | tee ../readrate-02-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) clustered clustered 1 $(DUR) $(SIZE) 10 2>&1 | tee ../readrate-03-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log
	cd build; $(CLEAN); $(CG) $(TEST) $(PR) readrate $(TR) clustered clustered 1 $(DUR) $(SIZE) 5 2>&1 | tee ../readrate-04-`hostname`-`date +%Y-%m-%d--%H-%M-%S`.log


build/%.o: %.cpp $(headers)
	mkdir -p build
	$(CXX) -c $< $(include_opt) $(debug_opts) -o $@
