#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
//#include <bits/stdc++.h> 
using namespace std;

int main(){
    string line;
    int i=0;
    char ch[30];
    int bytes_read_into_cache_reader=0, bytes_read_into_cache_writer=0, lock_wait_time_writer=0;
    int page_disk_to_cache_writer=0, schema_lock_wait_writer=0, schema_lock_wait_reader=0;
    int box_preloaded_reader=0, box_preloaded_writer=0, blocks_read_reader=0, blocks_read_writer=0;
    int blocks_written=0;
    bool bloom_falsepositive=true, bloom_hits=true, bloom_misser=true, bloom_evicted=true;
    bool bloom_pages_read_cache=true;
    ifstream myfile ("./build/wt_stat.log");
    if (myfile.is_open())
    {
        while ( getline (myfile,line) )
        {
            if(line.compare(0,37,"reader session: bytes read into cache")==0){
                stringstream check1(line);
                string intermediate;i=0;
                    while(getline(check1, intermediate, ' ')) 
                    {
                        i++;
                        if(i==8) strcpy(ch,intermediate.c_str());
                    }
                    for(i=0;i<strlen(ch)-2;i++){
                        ch[i]=ch[i+1];
                    }
                    ch[i]='\0';
                    bytes_read_into_cache_reader+=atoi(ch);
            }
            else if(line.compare(0,37,"writer session: bytes read into cache")==0){
                stringstream check1(line);
                string intermediate;i=0;
                    while(getline(check1, intermediate, ' ')) 
                    {
                        i++;
                        if(i==8) strcpy(ch,intermediate.c_str());
                    }
                    for(i=0;i<strlen(ch)-2;i++){
                        ch[i]=ch[i+1];
                    }
                    ch[i]='\0';
                    bytes_read_into_cache_writer+=atoi(ch);
            }
            else if(line.compare(0,38,"writer session: dhandle lock wait time")==0){
                stringstream check1(line);
                string intermediate;i=0;
                    while(getline(check1, intermediate, ' ')) 
                    {
                        i++;
                        if(i==8) strcpy(ch,intermediate.c_str());
                    }
                    
                    lock_wait_time_writer += atoi(ch);
            }
            else if(line.compare(0,49,"writer session: page read from disk to cache time")==0){
                stringstream check1(line);
                string intermediate;i=0;
                    while(getline(check1, intermediate, ' ')) 
                    {
                        i++;
                        if(i==11) strcpy(ch,intermediate.c_str());
                    }
                    
                    page_disk_to_cache_writer += atoi(ch);
            }
            else if(line.compare(0,37,"reader session: schema lock wait time")==0){
                stringstream check1(line);
                string intermediate;i=0;
                    while(getline(check1, intermediate, ' ')) 
                    {
                        i++;
                        if(i==9) strcpy(ch,intermediate.c_str());
                    }
                    for(i=0;i<strlen(ch)-2;i++){
                        ch[i]=ch[i+1];
                    }
                    ch[i]='\0';
                    schema_lock_wait_reader += atoi(ch);
            }
            else if(line.compare(0,37,"writer session: schema lock wait time")==0){
                stringstream check1(line);
                string intermediate;i=0;
                    while(getline(check1, intermediate, ' ')) 
                    {
                        i++;
                        if(i==9) strcpy(ch,intermediate.c_str());
                    }
                    for(i=0;i<strlen(ch)-2;i++){
                        ch[i]=ch[i+1];
                    }
                    ch[i]='\0';
                    schema_lock_wait_writer += atoi(ch);
            }
            else if(line.compare(0,39,"reader block-manager: blocks pre-loaded")==0){
                stringstream check1(line);
                string intermediate;i=0;
                    while(getline(check1, intermediate, ' ')) 
                    {
                        i++;
                        if(i==5) strcpy(ch,intermediate.c_str());
                    }
                    box_preloaded_reader += atoi(ch);
            }
            else if(line.compare(0,39,"writer block-manager: blocks pre-loaded")==0){
                stringstream check1(line);
                string intermediate;i=0;
                    while(getline(check1, intermediate, ' ')) 
                    {
                        i++;
                        if(i==5) strcpy(ch,intermediate.c_str());
                    }
                    box_preloaded_writer += atoi(ch);
            }
            else if(line.compare(0,33,"reader block-manager: blocks read")==0){
                stringstream check1(line);
                string intermediate;i=0;
                    while(getline(check1, intermediate, ' ')) 
                    {
                        i++;
                        if(i==5) strcpy(ch,intermediate.c_str());
                    }
                    blocks_read_reader += atoi(ch);
            }
            else if(line.compare(0,33,"writer block-manager: blocks read")==0){
                stringstream check1(line);
                string intermediate;i=0;
                    while(getline(check1, intermediate, ' ')) 
                    {
                        i++;
                        if(i==5) strcpy(ch,intermediate.c_str());
                    }
                    blocks_read_writer += atoi(ch);
            }
            else if(line.compare(0,36,"writer block-manager: blocks written")==0){
                stringstream check1(line);
                string intermediate;i=0;
                    while(getline(check1, intermediate, ' ')) 
                    {
                        i++;
                        if(i==5) strcpy(ch,intermediate.c_str());
                    }
                    blocks_written += atoi(ch);
            }
            else if(line.compare(0,40,"reader LSM: bloom filter false positives" && bloom_falsepositive)==0){
                bloom_falsepositive = false;
                count << line << endl;
            }
            else if(line.compare(0,29,"reader LSM: bloom filter hits" && bloom_hits)==0){
                bloom_hits = false;
                count << line << endl;
            }
            else if(line.compare(0,31,"reader LSM: bloom filter misses" && bloom_misser)==0){
                bloom_misser = false;
                count << line << endl;
            }
            else if(line.compare(0,49,"reader LSM: bloom filter pages evicted from cache" && bloom_evicted)==0){
                bloom_evicted = false;
                count << line << endl;
            }
            else if(line.compare(0,46,"reader LSM: bloom filter pages read into cache" && bloom_pages_read_cache)==0){
                bloom_pages_read_cache = false;
                count << line << endl;
            }
        }
        printf("Total bytes read into cache by readers: %d\n", bytes_read_into_cache_reader);
        printf("Total schema lock wait time (usecs) reader %d\n", schema_lock_wait_reader);
        printf("Total no. of blocks pre-loaded reader %d\n", box_preloaded_reader);
        printf("Total no. of blocks read reader %d\n", blocks_read_reader);

        printf("\nTotal bytes read into cache by writers: %d\n", bytes_read_into_cache_writer);
        printf("Total dhandle lock wait time (usecs) writers: %d\n", lock_wait_time_writer);
        printf("Total page read from disk to cache time (usecs) writer %d\n", page_disk_to_cache_writer);
        printf("Total schema lock wait time (usecs) writer %d\n", schema_lock_wait_writer);
        printf("Total no. of blocks pre-loaded writer %d\n", box_preloaded_writer);
        printf("Total no. of blocks read writer %d\n", blocks_read_writer);

        printf("\nTotal no. of blocks written: %d\n", blocks_written);
        myfile.close();
    }
    else cout<< "could not open file.\n";
    return 0;
}