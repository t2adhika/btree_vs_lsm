#ifndef _WIREDTIGER_H_
#define _WIREDTIGER_H_

#include <wiredtiger.h>
#include "main.h"

class WiredTigerManager : public Manager
{
    virtual std::unique_ptr<class Reader> newReader();
    virtual std::unique_ptr<class Writer> newWriter();
    virtual void dumpStoreStatistics();

public:
    WiredTigerManager();
    virtual ~WiredTigerManager();
    //WT_SESSION * session;
    WT_CONNECTION *conn;
};

class WiredTigerWriter : public Writer
{
    WT_CURSOR *cursor;
    WT_SESSION *session;

public:

    virtual ~WiredTigerWriter();    
    WiredTigerWriter( WiredTigerManager * manager );
    virtual void put( std::string const & key, std::string const & value ); 
};

class WiredTigerReader : public Reader
{
    WT_CURSOR *cursor;
    WT_SESSION *session;

public:

    virtual ~WiredTigerReader();
    WiredTigerReader( WiredTigerManager * manager );
    virtual std::string get( std::string const & key );
    virtual void * rangeBegin( std::string const & key_start, std::string const & key_end ); 
    virtual std::tuple<std::string,std::string> rangeNext( void * cursor );
    virtual void rangeEnd( void * cursor );
};

class WiredTigerCursor
{
    //char buffer_read[1024], buffer_write[1024];
    WT_CURSOR *cursor;
    WT_SESSION *session;
    bool in_range;
    //std:: string key_start, key_end;
    //WiredTiger::ReadOptions options;
    //WiredTiger::Slice
    std::string lower_bound, upper_bound;
    char key_start[64];
    //std::unique_ptr<WiredTiger::Iterator> iter;

    WiredTigerCursor( WiredTigerManager * manager, std::string const & key_start, std::string const & key_end );
    
    friend class WiredTigerReader;
};

#endif