#include <stdio.h>
#include <string.h>
#include <string>
#include <cassert>
#include <iostream>
#include <fstream>
#include "main.h"
#include "wt.h"
//#define WT_STAT_CONN_MEMORY_ALLOCATION 500

using namespace std;

void print_cursor(const char *th, WT_CURSOR *cursor);
int type;

int main(int argc, char *argv[])
{
    int i=0;
    type = atoi(argv[1]);
    for(i=1;i+1<argc;i++)
        argv[i] = argv[i+1];
    argc--;argv[i] = NULL;
    WiredTigerManager manager;
    return testmain(&manager, argc, argv);
}

WiredTigerManager::WiredTigerManager()
{
    assert( wiredtiger_open("db", NULL, "create,cache_size=4GB,statistics=(all)", &conn) == 0 );
    //conn->open_session(conn, NULL, NULL, &session);
}

WiredTigerManager::~WiredTigerManager()
{
    //WT_CURSOR *stat_cursor;   
   // session->open_cursor(session,"statistics:session", NULL, NULL, &stat_cursor);
}

void WiredTigerManager::dumpStoreStatistics()
{
    system("echo store,`du db | sed 's/\tdb//g'`");
}

unique_ptr<Reader> WiredTigerManager::newReader()
{
    return unique_ptr<Reader>( new WiredTigerReader( this ));
}

unique_ptr<Writer> WiredTigerManager::newWriter()
{   
        return unique_ptr<Writer>( new WiredTigerWriter( this ) );  
}

//-------------------------------------------------------

WiredTigerWriter::WiredTigerWriter(WiredTigerManager *manager):
    Writer(manager)
//    db( manager->db.get())
{
    manager->conn->open_session(manager->conn, NULL, NULL, &session);
    manager->conn->add_compressor(manager->conn,"comp",NULL,NULL);
    if(type == 0){
        session->create(session, "table:access_btree", "key_format=S,value_format=S,block_compressor=comp");
        session->open_cursor(session, "table:access_btree", NULL, "overwrite=false", &cursor);

    }    
    else{
        session->create(session, "table:access_lsm", "type=lsm,key_format=S,value_format=S,block_compressor=comp");
        session->open_cursor(session, "table:access_lsm", NULL, "overwrite=false", &cursor);
    }
}

WiredTigerWriter::~WiredTigerWriter()
{
    cursor->close(cursor);

    WT_CURSOR *curs;   
    //Database statistics
    session->open_cursor(session, "statistics:", NULL, NULL, &curs);
    print_cursor("writer",curs);
    curs->close(curs);

    //Table Statistics
    if(type==0) session->open_cursor(session, "statistics:table:access_btree", NULL, NULL, &curs);
    else session->open_cursor(session, "statistics:table:access_lsm", NULL, NULL, &curs);
    print_cursor("writer",curs);
    curs->close(curs);

    //Session Statistics
    session->open_cursor(session,"statistics:session", NULL, NULL, &curs);
    print_cursor("writer",curs);
    curs->close(curs);
    
    session->close(session,NULL);
}


void WiredTigerWriter::put( std::string const & key, std::string const & value )
{
    cursor->set_key(cursor, key.c_str());
    cursor->set_value(cursor, value.c_str());
    cursor->insert(cursor);
}

//-------------------------------------------------------

WiredTigerReader::WiredTigerReader( WiredTigerManager * manager ) :
    Reader( manager )
{
    manager->conn->open_session(manager->conn, NULL, NULL, &session);
    if(type == 0) session->open_cursor(session, "table:access_btree", NULL, NULL, &cursor);
    else session->open_cursor(session, "table:access_lsm", NULL, NULL, &cursor);
}
WiredTigerReader::~WiredTigerReader()
{
    cursor->close(cursor);
    
    WT_CURSOR *curs;   
    //Database statistics
    session->open_cursor(session, "statistics:", NULL, NULL, &curs);
    print_cursor("reader",curs);
    curs->close(curs);

    //Table Statistics
    if(type==0) session->open_cursor(session, "statistics:table:access_btree", NULL, NULL, &curs);
    else session->open_cursor(session, "statistics:table:access_lsm", NULL, NULL, &curs);
    print_cursor("reader",curs);
    curs->close(curs);

    //Session Statistics
    session->open_cursor(session,"statistics:session", NULL, NULL, &curs);
    print_cursor("reader",curs);
    curs->close(curs);
    
    session->close(session,NULL);
}

string WiredTigerReader::get( std::string const & key )
{
    char *value;

    cursor->set_key(cursor, key.c_str() );
    if((cursor->search(cursor))==0)
	cursor->get_value( cursor, &value );
    if(value!=NULL) {std::string str(value); return str;}
    return NULL;
}

/****************** FOR RANGE QUERIES ***********************/

WiredTigerCursor::WiredTigerCursor( WiredTigerManager * manager, string const & key_start, string const & key_end ) :
    lower_bound(key_start),
    upper_bound(key_end)
{
    manager->conn->open_session(manager->conn, NULL, NULL, &session);
    if(type==0) session->open_cursor(session, "table:access_btree", NULL, NULL, &cursor);
    else session->open_cursor(session, "table:access_lsm", NULL, NULL, &cursor);
    strncpy( this->key_start, key_start.c_str(), 64 );
    in_range = true;
}

void * WiredTigerReader::rangeBegin( string const & key_start, string const & key_end )
{
    WiredTigerCursor * wtc = new WiredTigerCursor( (WiredTigerManager*)manager, key_start, key_end );

    wtc->cursor->set_key(wtc->cursor, wtc->key_start );
    wtc->in_range = true;
    int result = -1;

    wtc->cursor->search_near( wtc->cursor, &result );
    if( result < 0 ) {
        wtc->in_range = false;
    }

    return wtc;
}

tuple<string,string> WiredTigerReader::rangeNext(void * c)
{
    int check=0;
    char *key, *value;
    WiredTigerCursor * wtc = (WiredTigerCursor*)c;

    if( wtc->in_range ) {
        //check = wtc->cursor->next(wtc->cursor);
        check = wtc->cursor->get_key(wtc->cursor, &key);
        check = wtc->cursor->get_value(wtc->cursor, &value);
        //printf("%s: %s\n",key,value);
        if(check==0) {
            tuple<string,string> ret ={key, value}; 
            if(std::get<0>( ret ) > wtc->upper_bound){
                wtc->in_range = false;
            }
            else {
                wtc->in_range = (wtc->cursor->next( wtc->cursor ) == 0 );
                return ret;
            }
        }    
        wtc->in_range = false;
    }
    return { "", "" };
}

void WiredTigerReader::rangeEnd( void * c )
{
    WiredTigerCursor * wtc = (WiredTigerCursor *)c;
    wtc->cursor->close(wtc->cursor);
    wtc->session->close(wtc->session,NULL);
    delete wtc;
}

/******************** RANGE QUERY ENDS **************************/

/*! [statistics display function] */
void print_cursor(const char *th, WT_CURSOR *cursor)
{
    const char *desc, *pvalue;
    int64_t value;
    int ret;
    ofstream myfile;
    myfile.open ("wt_stat.log",  std::ios_base::app);

    while ((ret = cursor->next(cursor)) == 0) {
        cursor->get_value(cursor, &desc, &pvalue, &value);
        if (value != 0){
            myfile <<th<< " " <<desc<< " " <<pvalue<<endl;
            printf("%s: %s=%s\n", th,desc, pvalue);

        }
    }
    myfile.close();
}
/*! [statistics display function] */