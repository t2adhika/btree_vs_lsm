#ifndef _MAIN_H_
#define _MAIN_H_

#include <thread>
#include <mutex>
#include <cassert>
#include <vector>
#include <atomic>
#include <tuple>
#include <chrono>
#include <random>
#include <unistd.h>

// These primes are for scattered ordering of keys
// Keys are generated as N * BIG_PRIME % 2^32
// This makes it easy to randomize keys, but still know which keys exist
const long BIG_PRIME = 2791728769L;  // First prime > 2^31 * 1.3
const long MED_PRIME = 2726299L;     // First prime > 2^21 * 1.3
const long SMALL_PRIME = 1361L;      // First prime > 2^10 * 1.3

extern int testmain( class Manager * manager, int argc, char *argv[] );

struct Stats
{
    std::chrono::time_point<std::chrono::high_resolution_clock> start_time;
    std::chrono::time_point<std::chrono::high_resolution_clock> end_time;
    std::chrono::duration<double> execution_time;
    long read_count = 0;
    long bytes_read = 0;
    long write_count = 0;
    long bytes_written = 0;

    static void dumpFormat();
    void dumpStatistics( std::string context );
};

enum KeyStrategy
{
    SCATTERED,
    CLUSTERED,
    SEQUENTIAL,
    RANGED
};

enum ThrottlingMethod
{
    DO_NOTHING,
    SLEEP_AFTER_OPS,
    RW_RATIO
};

class Manager
{
    std::random_device seeder;
    std::default_random_engine generator;
    std::uniform_int_distribution<long> distribution;

    std::thread monitor;
    bool running = false;
    std::vector<std::unique_ptr<class Writer>> writers;
    std::vector<std::unique_ptr<class Reader>> readers;
    int next_writer_id = 10000;

    virtual std::unique_ptr<class Reader> newReader() = 0;
    virtual std::unique_ptr<class Writer> newWriter() = 0;
    virtual void dumpStoreStatistics();
    static void monitorStatistics( Manager * manager );

public:
    std::mutex stats_lock;
    Stats stats;
    long total_writes = 0;
    long max_key_written = 0;

    long rand();
    class Writer * getRandomWriter();
    void startMonitor();
    void startAll();
    void stopAll();
    void dumpStatistics(std::string context);
    class Reader * makeReader( int reads_per_interval, int sleep_us, KeyStrategy strategy );
    class Reader * makeReader( int reads_per_interval, int sleep_us, int range_size );
    class Writer * makeWriter( int value_size, int sleep_us, int rw_ratio, KeyStrategy strategy, ThrottlingMethod t );
    void freeReader( class Reader * reader );
    void freeWriter( class Writer * writer );

    Manager();
    virtual ~Manager();
};

class Reader
{
    Stats stats;
    int reads_per_interval;
    int sleep_us;
    KeyStrategy key_strategy;
    int range_size;
    ThrottlingMethod throttling_method;

    std::atomic_bool running;
    std::thread thread;

    void doGets();
    void doRangeReads();
    void recordRead( std::string key, std::string value, std::chrono::duration<double> duration );
    static void worker( Reader * reader );
    virtual std::tuple<std::string,std::string> rangeNext( void * cursor ) = 0;
    virtual std::string get( std::string const & key ) = 0;     

    friend class Manager;

protected:
    Manager * const manager;

public:
    std::string getValue( std::string const & key );     
    virtual void * rangeBegin( std::string const & key_start, std::string const & key_end ) = 0; 
    std::tuple<std::string,std::string> getRangeNext( void * cursor );
    virtual void rangeEnd( void * cursor ) = 0;

    Reader( Manager * manager );
    virtual ~Reader();
    void start();
    void stop();
};

class Writer
{
    int value_size;
    int sleep_us;
    KeyStrategy key_strategy;

    Stats stats;
    Manager * const manager;
    std::atomic_bool running;
    std::thread thread;
    std::string key_prefix;
    long next_key_ordinal = 0;
    std::string buffer;

    virtual void put( std::string const & key, std::string const & value ) = 0;
    void doWrite(); 
    void doWrites();
    static void worker( Writer * updater );
    friend class Manager;

public:
    ThrottlingMethod throttling_method;
    long wrapAtOrdinal = __LONG_MAX__;    // Set to a lower value to for updates instead of inserts
    long max_key_ordinal;
    int rw_ratio;
    long ordinalToKey( long ordinal );
    Writer( Manager * manager );
    virtual ~Writer();
    int getMaxKeyOrdinal();
    std::string getKey( long ordinal );
    std::string keyToString( long key );
    void put( long key, std::string value );
    void putMany( long count );
    void start();
    void stop();
};

#endif
