#include "main.h"
#include <iostream>
#include <cstdlib>
#include <cmath>

using namespace std;

Manager::Manager() :
    generator( seeder() ),
    distribution( 0, 0x7FFFFFFFFFFFFFFF )
{    
}

long Manager::rand()
{
    unique_lock<mutex> lock( stats_lock ); 
    return distribution(generator);
}

Writer * Manager::getRandomWriter()
{
    return writers[rand() % writers.size()].get();
}

void Manager::startMonitor()
{
    if( !running ) {
        running = true;
        stats = Stats();  // Reset stats
        stats.start_time = chrono::high_resolution_clock::now();
        monitor = thread(monitorStatistics,this);
    }
}

void Manager::startAll()
{
    if( !running ) {
        running = true;
        stats = Stats();  // Reset stats
        stats.start_time = chrono::high_resolution_clock::now();
        monitor = thread(monitorStatistics,this);
    }

    for( auto const & writer : writers ) {
        writer->start();
    }
    for( auto const & reader : readers ) {
        reader->start();
    }

}

void Manager::stopAll()
{
    if( running ) {
        stats.end_time = chrono::high_resolution_clock::now();
        running = false;
        usleep(10000000);
        for( auto const & writer : writers ) {
            writer->stop();
        }
        for( auto const & reader : readers ) {
            reader->stop();
        }
        monitor.join();
        dumpStatistics( "total" );
    }
}

void Manager::freeReader( class Reader * reader )
{
    for( auto i = readers.begin(); i != readers.end(); ) {
        if( i->get() == reader ) {
            readers.erase(i);
        } else {
            i++;
        }
    }
}

void Manager::freeWriter( class Writer * writer )
{
    for( auto i = writers.begin(); i != writers.end(); ) {
        if( i->get() == writer ) {
            writers.erase(i);
        } else {
            i++;
        }
    }
}

class Reader * Manager::makeReader( int reads_per_interval, int sleep_us, KeyStrategy strategy )
{
    unique_ptr<Reader> reader = newReader();
    reader->throttling_method = SLEEP_AFTER_OPS;
    reader->reads_per_interval = reads_per_interval;
    reader->sleep_us = sleep_us;
    reader->key_strategy = strategy;
    reader->range_size = 1;
    Reader * ret = reader.get();
    readers.push_back( move(reader) );
    return ret;
}

class Reader * Manager::makeReader( int reads_per_interval, int sleep_us, int range_size )
{
    unique_ptr<Reader> reader = newReader();
    reader->throttling_method = SLEEP_AFTER_OPS;
    reader->reads_per_interval = reads_per_interval;
    reader->sleep_us = sleep_us;
    reader->key_strategy = CLUSTERED;
    reader->range_size = range_size;
    Reader * ret = reader.get();
    readers.push_back( move(reader) );
    return ret;
}

class Writer * Manager::makeWriter( int value_size, int sleep_us, int rw_ratio, KeyStrategy strategy, ThrottlingMethod t )
{
    unique_ptr<Writer> writer = newWriter();
    writer->throttling_method = t;
    writer->value_size = value_size;
    writer->sleep_us = sleep_us;
    writer->rw_ratio = rw_ratio;
    writer->key_strategy = strategy;
    writer->key_prefix = string("w" + to_string(next_writer_id++) + "_");
    writer->buffer.reserve(value_size + 1);
    
    char c[2];
    c[1] = 0;
    for( int i = 0; i < value_size; i++) {
        c[0] = rand();
        if( c[0] == 0 ) c[0] = 1;
        writer->buffer.append( c );
    }   
    
    Writer * ret = writer.get();
    writers.push_back( move(writer) );
    return ret;
}

Manager::~Manager()
{
    stopAll();
}

void Manager::dumpStatistics( string context )
{
    stats.dumpStatistics( context );
    system( "echo memory,usage,`cat /sys/fs/cgroup/memory/perftest/memory.usage_in_bytes`" );
    system( "echo memory,limit,`cat /sys/fs/cgroup/memory/perftest/memory.limit_in_bytes`" );
    cout << "misc,max_key_written," << to_string(max_key_written) << endl;
    dumpStoreStatistics();
}

void Manager::monitorStatistics( Manager * manager )
{
    usleep(5000000);
    while(manager->running) {
        manager->stats.end_time = chrono::high_resolution_clock::now();
        manager->dumpStatistics( "intermediate_total" );
        usleep(5000000);
    }
}

void Manager::dumpStoreStatistics()
{
}

void Stats::dumpFormat()
{
    cout << "legend" << "," 
        << "timer" << ","
        << "start" << ","
        << "end" << ","
        << "duration" << "," 
        << "execution_time" << ","
        << "read_count" << ","
        << "bytes_read" << ","
        << "write_count" << ","
        << "bytes_written" << endl;
}

void Stats::dumpStatistics( string context )
{
    char start_buf[20];
    char end_buf[20];
    std::time_t start = std::chrono::system_clock::to_time_t( start_time );
    std::time_t end = std::chrono::system_clock::to_time_t( end_time );
    strftime(start_buf, 20, "%m-%d %H:%M:%S", localtime( &start ) );
    strftime(end_buf, 20, "%m-%d %H:%M:%S", localtime( &end ) );
    double duration = (double)(end_time.time_since_epoch().count() - start_time.time_since_epoch().count()) / (double)1000000000;

    cout << "timer" << "," 
        << context << "," 
        << start_buf << ","
        << end_buf << ","
        << duration << "," 
        << execution_time.count() << ","
        << read_count << ","
        << bytes_read << ","
        << write_count << ","
        << bytes_written << endl;
}

