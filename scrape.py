#!/usr/bin/python
import sys
import os
import csv

testname = sys.argv[1].split('-',1)[0]
legend = ''
suite_context = '<uninitialized>'
context = '<uninitialized>'

for filename in os.listdir('.'):
    if filename.startswith(testname) and filename.endswith('.log'):
        with open( filename ) as csv_file :
            reader = csv.reader( csv_file, delimiter=',' )
            for line in reader :
                try :
                    if line[0] == 'legend' and line[1] == 'suite_begin' and legend != None:
                        legend = 'testname,' + ','.join( line[2:] )
                    if line[0] == 'legend' and line[1] == 'begin' and legend != None:
                        legend = legend + ',' + ','.join( line[2:] )
                    if line[0] == 'legend' and line[1] == 'timer' and legend != None:
                        print( legend + ',' + ','.join( line[2:] ) )
                        legend = None
                    if line[0] == 'status' and line[1] == 'suite_begin' :
                        suite_context = testname + ',' + ','.join( line[2:] )
                    if line[0] == 'status' and line[1] == 'begin' :
                        context = ','.join( line[2:] )
                    if line[0] == 'status' and line[1] == 'end' :
                        context = '<uninitialized>'
                    if line[0] == 'timer' and line[1] == 'total' :
                        print( filename + ',' + suite_context + ',' + context + ',' + ','.join( line[2:] ) )
                except Exception as e:
                    #print( str(e) + ":::" + ','.join(line) )
                    # ignore lines without a second element meaning line[1] undefined
                    pass

                

