#include "main.h"
#include <iostream>

using namespace std;

Reader::Reader( Manager * manager ) :
    manager( manager )
{
    running = false;
    stats.start_time = chrono::high_resolution_clock::now();
}

Reader::~Reader()
{
    stop();    
}

void Reader::start()
{
    if(!running) {
        stats = Stats();  // Reset stats
        stats.start_time = chrono::high_resolution_clock::now();
        running = true;
        thread = std::thread(worker, this);
    }
}

void Reader::stop()
{
    if( running ) {
        stats.end_time = chrono::high_resolution_clock::now();
        running = false;
        thread.join();
        stats.dumpStatistics( to_string( ((size_t)this) & 0xFFFF ) + "_reader" );
    }
}

void Reader::recordRead( string key, string value, chrono::duration<double> duration )
{
    stats.execution_time += duration;
    stats.read_count++;
    int size = value.size();
    stats.bytes_read += size;
    
    unique_lock<mutex> lock( manager->stats_lock ); 
    manager->stats.execution_time += duration;
    manager->stats.read_count++;
    manager->stats.bytes_read += size;
}

void Reader::doRangeReads()
{
    while(running) {
        Writer * writer = manager->getRandomWriter();

        if( writer->throttling_method == RW_RATIO ) {
            while( running ) {
                if( manager->stats.write_count > 0 ) {
                    int ratio = manager->stats.read_count / manager->stats.write_count;
                    if( ratio <= writer->rw_ratio ) {
                        break;
                    }
                }
                usleep( 10000 );
            }
        }

        long start_ordinal = manager->rand() % (writer->getMaxKeyOrdinal() - range_size );
        long start_key = writer->ordinalToKey( start_ordinal );

        void * cursor = rangeBegin( writer->keyToString(start_key), writer->keyToString( start_key + range_size ) );

        while(true) {
            auto pair = getRangeNext(cursor);
            if( std::get<1>( pair ).size()  == 0 ) {
                break;
            }
        }

        rangeEnd( cursor );
        usleep( sleep_us );
    }
}

string Reader::getValue( std::string const & key )
{
    chrono::time_point<chrono::high_resolution_clock> start = chrono::high_resolution_clock::now();
    string value = get( key );
    chrono::time_point<chrono::high_resolution_clock> end = chrono::high_resolution_clock::now();
    recordRead( key, value, end - start );
    return value;    
}

tuple<string,string> Reader::getRangeNext( void * cursor )
{
    chrono::time_point<chrono::high_resolution_clock> start = chrono::high_resolution_clock::now();
    auto pair = rangeNext( cursor );
    chrono::time_point<chrono::high_resolution_clock> end = chrono::high_resolution_clock::now();
    recordRead( std::get<0>( pair ), std::get<1>( pair ), end - start );
    return pair;    
}

void Reader::doGets()
{
    while(running) {
        Writer * writer = manager->getRandomWriter();

        if( writer->throttling_method == RW_RATIO ) {
            while( running ) {
                if( manager->stats.write_count > 0 ) {
                    int ratio = manager->stats.read_count / manager->stats.write_count;
                    if( ratio <= writer->rw_ratio ) {
                        break;
                    }
                }
                usleep( 10000 );
            }
        }
        
        int max_key_ordinal = writer->getMaxKeyOrdinal();

        switch( key_strategy ) 
        {
        case SEQUENTIAL: {
            int ordinal = manager->rand() % (max_key_ordinal - reads_per_interval);
            for( int i = 0; i < reads_per_interval; i++ ) {
                string key = writer->getKey( ordinal );
                string s = getValue( key );
                if( s.size() == 0 ) {
                    cout << "KEY NOT FOUND ordinal=" << ordinal << " key=" << key << " max_key_ordinal=" << max_key_ordinal << endl;
                    exit(1);
                }
                ordinal++;
            }
            break;
        }

        case CLUSTERED: {
            // Top 21 bits for the region combined with 10 random low bits
            int base_ordinal = 0;
            while( base_ordinal < 0x400 ) {
                max_key_ordinal = writer->getMaxKeyOrdinal();
                base_ordinal = ( manager->rand() % max_key_ordinal ) & 0x7FFFFC00;
                // Wait until enough values are present.  This won't terminate if there are no writers.
                usleep( 100000 );
            }
            base_ordinal = (base_ordinal - 0x400) & 0x7FFFFC00; 
            for( int i = 0; i < reads_per_interval; i++ ) {
                int ordinal = base_ordinal + (manager->rand() & 0x3FF);
                string key = writer->getKey( ordinal );
                string s = getValue( key );
                if( s.size() == 0 ) {
                    cout << "KEY NOT FOUND ordinal=" << ordinal << " key=" << key << " max_key_ordinal=" << max_key_ordinal << endl;
                    exit(1);
                }
            }
            break;
        }

        case SCATTERED:
            for( int i = 0; i < reads_per_interval; i++ ) {
                int ordinal = manager->rand() % max_key_ordinal;
                string key = writer->getKey( ordinal );
                string s = getValue( key );
                if( s.size() == 0 ) {
                    cout << "KEY NOT FOUND ordinal=" << ordinal << " key=" << key << " max_key_ordinal=" << max_key_ordinal << endl;
                    exit(1);
                }
            }
            break;

        default:
            break;
        }

        if( sleep_us > 0 ) {
            usleep( sleep_us );
        }
    }
}

void Reader::worker( Reader * reader )
{
    if( reader->range_size == 1 ) {
        reader->doGets();
    } else {
        reader->doRangeReads();
    }
}


