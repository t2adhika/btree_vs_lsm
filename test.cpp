#include <iostream>
#include <string>
#include <cassert>

#include "main.h"

using namespace std;

int scale_factor = 1;

string strategyStr( KeyStrategy k )
{
    switch( k ) {
        case SCATTERED:
            return "scattered";
        case CLUSTERED:
            return "clustered";
        case SEQUENTIAL:
            return "sequential";
        case RANGED:
            return "ranged";
        default:
            assert( false );
    }
}

void testReadRate( Manager * manager, Writer * writer, int seconds, long key_count )
{
    long add_key_count = key_count - manager->total_writes;

    if( add_key_count > 0 ) {
        cout << "status,prepare,add," << add_key_count << endl;
        writer->putMany(add_key_count);
    }
    
    cout << "info,Begin " << seconds << " seconds of reads - " << manager->total_writes << " keys." << endl;
    cout << "status,begin," << key_count << endl;
    manager->startAll();
    usleep( 1000000 * seconds );
    manager->stopAll();
    cout << "status,end" << endl;
}

void testReadRates( Manager * manager, Writer * writer, int readers,
                            int seconds, int value_size, int rw_ratio )
{
    testReadRate( manager, writer, seconds, 100000 * scale_factor );
    testReadRate( manager, writer, seconds, 500000 * scale_factor );
    testReadRate( manager, writer, seconds, 1000000 * scale_factor );
    testReadRate( manager, writer, seconds, 1500000 * scale_factor );
    testReadRate( manager, writer, seconds, 2000000 * scale_factor );
    testReadRate( manager, writer, seconds, 5000000 * scale_factor );
}

void testReadRates( Manager * manager, int readers, 
                            KeyStrategy read_strategy, KeyStrategy write_strategy, int read_range_size,
                            int seconds, int value_size, int rw_ratio )
{
    cout << "info,Begin testing random reads with value size " 
        << value_size << " and a read:write ratio of " << rw_ratio 
        << ".  Using " << readers << " reader threads." << endl;

    cout << "info,Scale factor " << scale_factor << endl;

    cout << "legend,suite_begin,readers,read_strategy,write_strategy,read_range_size," 
         << "seconds,value_size,rw_ratio" << endl;

    cout << "legend,begin,key_count" << endl;

    Stats::dumpFormat();

    cout << "status,suite_begin," << readers << "," 
        << strategyStr(read_strategy) << "," << strategyStr(write_strategy) << "," << read_range_size << ","
        << seconds << "," << value_size << "," << rw_ratio << endl;

    Writer * writer = manager->makeWriter( value_size, 1, rw_ratio, write_strategy, RW_RATIO );

    for( int i = 0; i < readers; i++ ) {
        if( read_strategy == RANGED ) {
            manager->makeReader( 1000, 0, read_range_size );
        } else {
            manager->makeReader( 1000, 0, read_strategy );
        }
    }

    testReadRates( manager, writer, readers, seconds, value_size, rw_ratio );
}

void testReadRates2( Manager * manager, int readers, 
                            KeyStrategy read_strategy, KeyStrategy write_strategy, int read_range_size,
                            int seconds, int value_size, int rw_ratio )
{
    // Record expected prepopulated state
    if( write_strategy == SCATTERED ) {
        manager->total_writes = 20000000;
    } else {
        manager->total_writes = 200000000;
    }

    cout << "info,Begin testing random reads with value size " 
        << value_size << " and a read:write ratio of " << rw_ratio 
        << ".  Using " << readers << " reader threads." << endl;

    cout << "info,Scale factor " << scale_factor << endl;

    cout << "legend,suite_begin,readers,read_strategy,write_strategy,read_range_size," 
         << "seconds,value_size,rw_ratio" << endl;

    cout << "legend,begin,key_count" << endl;

    Stats::dumpFormat();

    cout << "status,suite_begin," << readers << "," 
        << strategyStr(read_strategy) << "," << strategyStr(write_strategy) << "," << read_range_size << ","
        << seconds << "," << value_size << "," << rw_ratio << endl;

    Writer * writer = manager->makeWriter( value_size, 1, rw_ratio, write_strategy, RW_RATIO );
    writer->wrapAtOrdinal = manager->total_writes;
    writer->max_key_ordinal = manager->total_writes;

    for( int i = 0; i < readers; i++ ) {
        if( read_strategy == RANGED ) {
            manager->makeReader( 1000, 0, read_range_size );
        } else {
            manager->makeReader( 1000, 0, read_strategy );
        }
    }

    testReadRate( manager, writer, seconds, manager->total_writes );
}

void populate( Manager * manager, KeyStrategy write_strategy, long write_count, int value_size )
{
    Writer * writer = manager->makeWriter( value_size, 1, 1, write_strategy, DO_NOTHING );
    cout << "status,prepare,add," << write_count << endl;
    manager->startMonitor();
    writer->putMany(write_count);
    manager->stopAll();
}

void validateImplementation( Manager * manager )
{
    Writer * writer = manager->makeWriter( 100, 100, 1, SEQUENTIAL, SLEEP_AFTER_OPS );
    for( int i = 0; i < 100; i++ ) {
        if( i == 10 ) continue;
        writer->put( i, "testvalue" );
    }

    Reader * reader = manager->makeReader( 100, 100, 1 );

    void * cursor = reader->rangeBegin( writer->keyToString(10), writer->keyToString(15) );
    int found = 0;
    while( true ) {
        auto pair = reader->getRangeNext( cursor );
        string key = get<0>( pair ); 
        if( key.size() == 0 ) {
            break;
        }
        found++;
        cout << key << "=" << get<1>( pair ) << endl;
    }
    assert( found == 5 );
    reader->rangeEnd( cursor );

    cursor = reader->rangeBegin( writer->keyToString(101), writer->keyToString(105) );
    found = 0;
    while( true ) {
        auto pair = reader->getRangeNext( cursor );
        string key = get<0>( pair ); 
        if( key.size() == 0 ) {
            break;
        }
        found++;
        cout << key << "=" << get<1>( pair ) << endl;
    }
    assert( found == 0 );
    reader->rangeEnd( cursor );

    cursor = reader->rangeBegin( writer->keyToString(98), writer->keyToString(102) );
    found = 0;
    while( true ) {
        auto pair = reader->getRangeNext( cursor );
        string key = get<0>( pair ); 
        if( key.size() == 0 ) {
            break;
        }
        found++;
        cout << key << "=" << get<1>( pair ) << endl;
    }
    assert( found == 2 );
    reader->rangeEnd( cursor );

    manager->freeReader( reader );
    manager->freeWriter( writer );
}

void expect( int expected, int actual )
{
    if( expected != actual ) {
        cout << "Wrong number of arguments: expected " << expected << " got " << actual << endl;
        exit(1);
    }
}

KeyStrategy parseStrategy( std::string const & strategy )
{
    if( strategy == "scattered" ) return SCATTERED;
    if( strategy == "clustered" ) return CLUSTERED;
    if( strategy == "sequential" ) return SEQUENTIAL;
    if( strategy == "ranged" ) return RANGED;
    cout << "Invalid key strategy: " << strategy << endl;
    exit(1);
}

int testmain( Manager * manager, int argc, char *argv[] )
{
    manager->stats.start_time = chrono::high_resolution_clock::now();

    if( argc <= 1 ) {
        validateImplementation( manager );
        return 0;
    }

    string testname( argv[1] );

    if( testname == "readrate" || testname == "readrate2" ) {
        expect( 9, argc );
        int reader_threads = atoi( argv[2] );
        KeyStrategy read_strategy = parseStrategy( argv[3] );
        KeyStrategy write_strategy = parseStrategy( argv[4] );
        int read_range_size = atoi( argv[5] );
        int duration = atoi( argv[6] );
        int value_size = atoi( argv[7] );
        int rw_ratio = atoi( argv[8] );
        if( testname == "readrate" ) {
            testReadRates( manager, reader_threads, read_strategy, write_strategy, read_range_size, duration, value_size, rw_ratio );
        } else {
            testReadRates2( manager, reader_threads, read_strategy, write_strategy, read_range_size, duration, value_size, rw_ratio );            
        }
    } else if( testname == "populate" ) {
        expect( 5, argc );
        KeyStrategy write_strategy = parseStrategy( argv[2] );
        long write_count = atol( argv[3] );
        int value_size = atoi( argv[4] );
        populate( manager, write_strategy, write_count, value_size );
    } else {
        cout << "Unknown test: " << testname << endl;
        return 1;
    }
    
    return 0;
}
